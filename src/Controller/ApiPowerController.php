<?php

namespace App\Controller;

use App\Entity\Power;
use App\Form\PowerType;
use App\Repository\PowerRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/power", name="api_power")
 */
class ApiPowerController extends AbstractController
{
    /**
     * @Route(methods="GET")
     */
    public function index(PowerRepository $repo)
    {
      
        return $this->json($repo->findBy([]));
    }
    /**
     * @Route("/{id}", methods="GET")
     */
    public function one(Power $power)
    {
        return $this->json($power);
    }

    /**
     * @Route("/{id}", methods="DELETE")
     */
    public function delete(Power $power, ObjectManager $manager)
    {
        $manager->remove($power);
        $manager->flush();
        
        return $this->json(null, 204);
    }

    /**
     * @Route("/{id}", methods="PATCH")
     * @Route(methods="POST")
     */
    public function add(Power $power = null, Request $request, ObjectManager $manager)
    {
        
        if (!$power) {
            $power = new Power();
        }
        $form = $this->createForm(PowerType::class, $power);
    
      
        $form->submit(
           
            json_decode(
                $request->getContent(),
                true
            ), 
            false
        );

        if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($power);
            $manager->flush();
      
            return $this->json($power, 201);
        }
      
        return $this->json($form->getErrors(true), 400);
    }
}
