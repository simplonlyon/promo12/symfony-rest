<?php

namespace App\Controller;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Le fait de mettre la route sur le controller directement fait que 
 * toutes les routes définies à l'intérieur de ce controller partageront
 * le chemin définie ici comme préfix
 * @Route("/api/hero", name="api_hero")
 */
class ApiHeroController extends AbstractController
{
    /**
     * Ici on ne définit pas de chemin pour la route, elle prend donc
     * uniquement le préfix défini sur le controller (/api/hero), on
     * indique juste que cette méthode ne sera accessible que par
     * une méthode HTTP GET
     * @Route(methods="GET")
     */
    public function index(HeroRepository $repo)
    {
        //On renvoie une réponse http json dans laquelle on symfony
        //va convertir en JSON les instances qu'on lui passe en argument
        return $this->json($repo->findBy([]));
    }
    /**
     * @Route("/{id}", methods="GET")
     */
    public function one(Hero $hero)
    {
        return $this->json($hero);
    }

    /**
     * @Route("/{id}", methods="DELETE")
     */
    public function delete(Hero $hero, ObjectManager $manager)
    {
        $manager->remove($hero);
        $manager->flush();
        //En rest, il est de bon aloi de tirer parti des code de retour
        //http, ici 204 veut dire "succès mais pas de contenu renvoyé"
        return $this->json(null, 204);
    }

    /**
     * On peut mettre deux routes différentes sur une même méthode, même
     * si, comme ici, l'une d'elle a un paramètre de route, il suffira alors
     * de mettre une valeur par défaut à null à ce paramètre dans la méthode
     * pour faire que ça marche également dans le cas du post
     * @Route("/{id}", methods="PATCH")
     * @Route(methods="POST")
     */
    public function add(Hero $hero = null, Request $request, ObjectManager $manager)
    {
        //La méthode d'ajout ressemble à 85% à celle qu'on ferait avec du twig
        if (!$hero) {
            $hero = new Hero();
        }
        $form = $this->createForm(HeroType::class, $hero);
        //On ne fait pas un handleRequest, mais à la place on submit manuellement le form
        //La méthode submit attend un tableau associatif contenant les clef et valeur du form
        $form->submit(
            //On utilise la méthode json_decode pour transformer le contenu de la
            //request (qui est une chaîne de caractère json) en tableau associatif
            json_decode(
                $request->getContent(),
                true
            ), 
            false
        );

        if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($hero);
            $manager->flush();
            //Si on arrive ici, c'est que c'est ok, on renvoie un 201 pour "success created"
            return $this->json($hero, 201);
        }
        //Si on arrive là c'est qu'on est pas passé dans le if, à ce moment
        //on renvoie un 400 "bad request" avec les erreurs du formulaire
        return $this->json($form->getErrors(true), 400);
    }



    /**
     * Ici, on définit un chemin, donc la méthode en question sera
     * accessible via une méthode HTTP GET sur le chemin /api/hero/test
     * @Route("/test", methods="GET") 
     */
    public function test()
    {

        //Les trois return suivant font presqu'exactement la même chose mais
        //différement, je conseil d'utiliser la dernière version, non commentée
        //avec $this->json()

        /**
         * Lorsqu'on envoie une response http contenant du json, il faut
         * indiquer que la réponse en question aura dans ses headers une
         * propriété Content-Type qui indique au client le format des
         * données qu'il va recevoir, ici, du json
         */
        // return new Response('{"id":1, "name":"test"}', 200, [
        //     'Content-Type' => 'application/json'
        // ]);
        /**
         * Le fait de créer une response http avec le header content-type json
         * peut être fait automatiquement par symfony en utilisant une JsonResponse
         * qui, en plus de mettre ce header, va également convertir en json les 
         * données/objets/tableaux php qu'on lui donnera en argument
         */
        // return new JsonResponse(["id" => 1, "name" => "test"]);
        /**
         * La méthode $this->json() disponible grâce à l'AbstractController va
         * automatiquement créer une nouvelle instance de JsonResponse pour nous
         */
        return $this->json(["id" => 1, "name" => "test"]);
    }
}
